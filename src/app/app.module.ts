import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector  } from '@angular/core';


import { AppComponent } from './app.component';
import { PublisherComponent } from './publisher/publisher.component';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { OpentokService } from './opentok.service';
import { createCustomElement } from '@angular/elements';
import { AngularDraggableModule } from 'angular2-draggable';


@NgModule({
  declarations: [
    AppComponent,
    PublisherComponent,
    SubscriberComponent,
  ],
  imports: [
    BrowserModule,
    AngularDraggableModule
  ],
  providers: [OpentokService],
  bootstrap: [AppComponent],
  entryComponents: [
    AppComponent
  ]
})

export class AppModule {
  constructor(private injector: Injector) {
  }
  ngDoBootstrap() {
    const myCustomElement = createCustomElement(AppComponent, { injector: this.injector });
    customElements.define('app-video-meet', myCustomElement);
  }
}
