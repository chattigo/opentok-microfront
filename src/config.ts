export default {
  // Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
  // Do not include the trailing slash. See the README for more information:
  SAMPLE_SERVER_BASE_URL: 'http://localhost:4200/',
  // OR, if you have not set up a web server that runs the learning-opentok-php code,
  // set these values to OpenTok API key, a valid session ID, and a token for the session.
  // For test purposes, you can obtain these from https://tokbox.com/account.
  API_KEY: '46809394',
  SESSION_ID: '1_MX40NjgwOTM5NH5-MTU5NDY5Mjg2NzY2MX5WN1lvVmlCZzBEYUIwM3podFp6ejhodVN-fg',
  TOKEN: 'T1==cGFydG5lcl9pZD00NjgwOTM5NCZzaWc9NWQ3NTlmOTRmNzQ4MTQ5YjRkNjRkN2MxYWZjY2M0YjJhZjRiZDAzMzpzZXNzaW9uX2lkPTFfTVg0ME5qZ3dPVE01Tkg1LU1UVTVORFk1TWpnMk56WTJNWDVXTjFsdlZtbENaekJFWVVJd00zcG9kRnA2ZWpob2RWTi1mZyZjcmVhdGVfdGltZT0xNTk0Nzc5NTg0Jm5vbmNlPTAuMDM4MjA1ODU5ODczNzk3NzY1JnJvbGU9cHVibGlzaGVyJmV4cGlyZV90aW1lPTE1OTQ3ODMwMjcmaW5pdGlhbF9sYXlvdXRfY2xhc3NfbGlzdD0='
};
